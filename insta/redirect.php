<!DOCTYPE html>
<html>
<head>
	<title>InstaEtty Project</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" width="20px" src="insta_logo.png" alt="App Icon">
      </a>
      <a class="navbar-brand">Insta viewer - Opvolger</a>
      <ul class="nav navbar-nav">
      <li></li>
      </ul>

    </div>
  </div>
</nav>

<style>
html { 
  background: url('https://wallpapercave.com/wp/wp1954103.jpg') no-repeat center center fixed; 
  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;
}
</style>
<?php
require_once('configuration.php');

if(!isset($_SESSION['user'])){
	$auth_token = $_GET['code'];
	$payload = getInstaPayload($auth_token);
	$response  = exchangeAuthToken($payload);
	if(isset($response->body->code) && $response->body->code == 400){
		header('Location: client.php');	
	}else{
		$user = buildInstaUser($response);
		$_SESSION['user'] = $user;	
	}
}else{
	header('Location: client.php');
}


$access_token = $_SESSION['user']['access_token'];
$url = 'https://api.instagram.com/v1/users/self/media/recent/?access_token=' . $access_token;
$response = \Httpful\Request::get($url)->expectsJson()->send();

// https://www.instagram.com/developer/endpoints/users/ hier kan je een "voorbeeld" RESPONSE bekijken.
?>

<h1> Al je recent data: </h1>
<?php
	echo '<pre>' . print_r($response, true) . '</pre>';
?>
<br/>

<h1> Eerste plaatje </h1>
<?php
	// Het "0"ste (dus eerste) plaatje ophalen uit je data (we kiezen low_resolution en willen weten waar deze staat op het internet (url))
	$imageurl = $response->body->data[0]->images->low_resolution->url;
	// dit is het eerste plaatjes
	echo '<img src="' . $imageurl . '">';
?>
<br/>

<h1> Alle plaatjes </h1>
<?php
	// Alle plaatsjes
	$plaatjesLengte = count($response->body->data);
	for ($i=0; $i<$plaatjesLengte; $i++) {
		echo '  <div class="col-sm-6 col-md-4">            ';
		echo '    <div class="thumbnail">            ';
		echo '      <img src="' . $response->body->data[$i]->images->low_resolution->url . '">            ';
		echo '      <div class="caption">            ';
		echo '        <h3>' . $response->body->data[$i]->caption->text .'</h3>            ';
		echo '        <p><a href="https://www.instagram.com/bas.topicus/?hl=nl" class="btn btn-primary" role="button">Profiel</a> </p>            ';
		echo '      </div>            ';
		echo '    </div>            ';
		echo '  </div>            ';
	}
?>

</body>
</html>